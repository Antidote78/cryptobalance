import sys
import json
import math
import os
import datetime
import time
import schedule
from influxdb import InfluxDBClient

USER = 'root'
PASSWORD = 'root'
DBNAME = 'portfolio'

root = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(root + '/python')
from forex_python.bitcoin import  BtcConverter

import ccxt  # noqa: E402

def get_tickers(coin,exchange):
	if (coin == 'BTC'):
		return 1
	if (coin == 'USDT'):
		return 0
	else:
		ticker = exchange.fetch_ticker(coin + '/BTC')
		return (ticker['high'] + ticker['low']) / 2

def get_btc_value(*balances):
	sum = 0
	for balance in balances:
		for coin in list(balance[0]['total']):
			newBalance = get_tickers(coin, balance[1]) * balance[0]['total'][coin]
			sum += newBalance
	return sum

def get_euro_value(btc):
	bitcoin_price = BtcConverter()
	value = bitcoin_price.get_latest_price('EUR')
	return btc * value

def insert_influxDB(bitcoin, euros, name):
	point = [{
		"measurement": "Portfolio_value",
		"tags": {
			"Investor": 'All'
		},
		"fields": {
			"Euro": euros,
			"BTC": bitcoin
		}
	}]
	return point


def get_balances(poloniex,name):
	try:

		poloniexBalances = poloniex.fetch_balance()
		for i in list(poloniexBalances['total']):
			if (poloniexBalances['total'][i] == 0):
				del poloniexBalances['total'][i]

		# Delete BittrexBalances if not used
		#bittrexBalances = bittrex.fetch_balance()
		#for i in list(bittrexBalances['total']):
		#	if (bittrexBalances['total'][i] == 0):
		#		del bittrexBalances['total'][i]


		# Delete bittrexBalances, bittrex here if not used.
		btc_value = get_btc_value([poloniexBalances, poloniex])
		eur_value = get_euro_value(btc_value)
		print('Sum = ' + str(btc_value))
		print('EUR = ' + str(eur_value))
		point =  insert_influxDB(btc_value, eur_value, name)
		return point

	except ccxt.DDoSProtection as e:
		print(type(e).__name__, e.args, 'DDoS Protection (ignoring)')
	except ccxt.RequestTimeout as e:
		print(type(e).__name__, e.args, 'Request Timeout (ignoring)')
	except ccxt.ExchangeNotAvailable as e:
		print(type(e).__name__, e.args, 'Exchange Not Available due to downtime or maintenance (ignoring)')
	except ccxt.AuthenticationError as e:
		print(type(e).__name__, e.args, 'Authentication Error (missing API keys, ignoring)')


def main(host='localhost', port=8086):

	poloniex = ccxt.poloniex({
	'apiKey': "DHS59XA0-5NZLNRFY-MPIUL8UD-FVMWAJVE",
	'secret': "49a4a74a608ce113b40277ae4199db6c1d6b8df52e8dd76e7c2febb589133c96093a24cf07f2250cbb715985646f049a5fd25e5663a8531a153a7061c5004cc6",
	'verbose': False,  # switch it to False if you don't want the HTTP log
	})

	#Remove bittrex if not used
	#bittrex = ccxt.bittrex({
	#'apiKey': "",
	#'secret': "",
	#'verbose': False,  # switch it to False if you don't want the HTTP log
	#})


	client = InfluxDBClient(host, port, USER, PASSWORD, DBNAME)
	client.create_database(DBNAME)
	client.switch_database(DBNAME)

	# Write points
	## remove bittrex if not used
	point = get_balances(poloniex, "Stan")
	print("Adding to database")
	client.write_points(point)

schedule.every(0.1).minutes.do(main)
while True:
	schedule.run_pending()


